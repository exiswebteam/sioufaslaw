jQuery( window ).load(function() {
  runmasonry();
});

jQuery( document ).on( 'js_event_wpv_pagination_completed js_event_wpv_parametric_search_results_updated', function( event, data ) {
  runmasonry();
});

function runmasonry() {
  jQuery('.masonry-grid').masonry({
    // options
    gutter: 0,
    itemSelector: '.grid-item'
  });
}

