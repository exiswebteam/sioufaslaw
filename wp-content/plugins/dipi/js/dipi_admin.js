(function($){
  $(window).load(function() {
    console.log('loaded');
    $('body').on('DOMNodeInserted DOMSubtreeModified', '.js-wpt-file-preview', function() {
      console.log('changed');
      // This black magic part keeps only the filename (stripping path and extension)
      var imageName = $(this).prev().children('input').val().replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/, "");
      // Really bad 
      $(this).parent().parent().parent().parent().prev().children('input').val(imageName);
    });
  });
})(jQuery)
