(function($){

  $(document).ready( function(){
    $('.transposh_flags a').each(function() {
      if ($(this).attr('href').indexOf('lang=ro') !== -1)
        $(this).attr('href', 'http://www.sioufaslaw.ro/');
    });
    if (getQueryVariable('lang')) {
      var language = getQueryVariable('lang');
      if (language === 'en') {
        $('#logo').attr('src', '/wp-content/uploads/2018/03/logoen.png');
      } else if (language === 'ro') {
        $('#logo').attr('src', '/wp-content/uploads/2018/03/logoro.png');
      }
    }

    // Newsletter
    var agree = $('<div class="nl-agree"><p><input type="checkbox" id="nl-agree-1" class="input" name="nl-agree-1"> <label class="nl-agree-lb" for="nl-agree-1">Συμφωνώ στη χρήση της ηλεκτρονικής μου διεύθυνσης με σκοπό την αποστολή ενημερωτικού υλικού (newsletter).</label></p><p><input type="checkbox" id="nl-agree-2" class="input" name="nl-agree-2"><label class="nl-agree-lb" for="nl-agree-2">Έχω διαβάσει και αποδέχομαι την Πολιτική Απορρήτου Ιστοσελίδας.</label></p></div>');
    agree.insertAfter('.sml_email');

    $('.sml_submit input').prop('disabled', true);

    $('form.sml_subscribe').on('change', 'input[name="nl-agree-1"]', function() {
      $('.sml_submit input').prop('disabled', cantSubscribeToNewsletter());
    });

    $('form.sml_subscribe').on('change', 'input[name="nl-agree-2"]', function() {
      $('.sml_submit input').prop('disabled', cantSubscribeToNewsletter());
    });

    function cantSubscribeToNewsletter() {
      var checkAgree1 = document.getElementById('nl-agree-1').checked;
      var checkAgree2 = document.getElementById('nl-agree-2').checked;
      return !(checkAgree1 && checkAgree2);
    }

  }); // End document ready

})(jQuery) // End jQuery

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0; i<vars.length; i++) {
    var pair = vars[i].split("="); 
    if(pair[0] == variable) {
      return pair[1]; 
    }
  }     
  return false;
}
