<?php
/**
 * Plugin Name: Digital Pioneers' Plugin
 * Plugin URI: http://digitalpioneers.gr
 * Description: Custom plugin to include the necessary js/css files without conflicts.
 * Version: 1.0.0
 * Author: Thomas Minitsios
 * Author URI: http://digitalpioneers.gr
 * License: GPL2
 */

defined( 'ABSPATH' ) or die( 'Huehuehue!' );

function dipi_scripts_basic() {
  // Register
  wp_register_script('custom-script', plugins_url('/js/dipi.js', __FILE__), array('jquery') ); // Has a jquery dependency

  // Enqueue
  wp_enqueue_script('custom-script');

}

function dipi_styles_basic() {
  // Register
  wp_register_style('custom-style', plugins_url('/css/dipi.css', __FILE__), array(), '20150512', 'all' );

  // Enqueue
  wp_enqueue_style('custom-style');

}

function dipi_head() {
  echo '<link rel="stylesheet" type="text/css" href="/wp-content/themes/sioufas/fonts.css" />';
}

add_action('wp_head', 'dipi_head');
add_action('wp_enqueue_scripts', 'dipi_scripts_basic');
add_action('wp_enqueue_scripts', 'dipi_styles_basic', 20);
