<?php

$labels = array(
    'name' => _x('CF7 Entries: Contact Form 7 Store to DB', 'post type general name', 'contact-form-7-store-to-db'),
    'singular_name' => _x('Contact Form 7 Store to DB', 'post type singular name', 'contact-form-7-store-to-db'),
    'menu_name' => _x('CF7 Entries', 'admin menu', 'contact-form-7-store-to-db'),
    'name_admin_bar' => _x('Contact Form 7 Store to DB', 'add new on admin bar', 'contact-form-7-store-to-db'),
    'add_new' => _x('New', 'Contact Form 7 Store to DB', 'contact-form-7-store-to-db'),
    'edit_item' => __('View Full Entry Details', 'contact-form-7-store-to-db'),
    'view_item' => __('&nbsp;', 'contact-form-7-store-to-db'),
    'all_items' => __('All Entries', 'contact-form-7-store-to-db'),
    'search_items' => __('Search Entries', 'contact-form-7-store-to-db'),
    'parent_item_colon' => __('Parent Entries:', 'contact-form-7-store-to-db'),
    'not_found' => __('No Entries Found.', 'contact-form-7-store-to-db'),
    'not_found_in_trash' => __('No Entries Found in Trash.', 'contact-form-7-store-to-db')
);

$args = array(
    'labels' => $labels,
    'description' => __('Description.', 'contact-form-7-store-to-db'),
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_icon' => 'dashicons-admin-users',
    'query_var' => true,
    'rewrite' => array('slug' => 'cf7stdb-entries'),
    'capability_type' => 'post',
    'capabilities' => array(
        'create_posts' => 'do_not_allow',
        'edit_posts' => true,
        'edit_post' => false,
        'delete_post' => true
    ),
    'map_meta_cap' => true,
    'has_archive' => false,
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => false
);