<?php

if (isset($_POST['cf7stdb_cf7_entries'])) {
    $cf7stdb_cf7_entries = (array) $_POST['cf7stdb_cf7_entries'];
    // sanitize array
    //$cf7stdb_cf7_entries = array_map('cf7stdb_cf7_entries', $cf7stdb_cf7_entries);
    // save data
    update_post_meta($post_id, 'cf7stdb_cf7_entries', $cf7stdb_cf7_entries);
}