<?php
defined('ABSPATH') or die('No script kiddies please!');
?>

<?php
$cf7_posts = $this->cf7stdb_contact_form_7_plugin_posts();
$cf7stdb_posts = $this->cf7stdb_contact_form_plugin_posts();
$cd7stdb_options = get_option('cf7stdb_settings');
$cf7_form_id = !empty($_GET['cf7_form_id']) ? esc_attr($_GET['cf7_form_id']) : '';
$entries_year_specific = !empty($_GET['cf7_year']) ? esc_attr($_GET['cf7_year']) : '2018';
$entries_month_specific = !empty($_GET['cf7_month']) ? esc_attr($_GET['cf7_month']) : '';
$entries_array = array();
foreach ($cf7stdb_posts as $cf7stdb_post_key => $cf7stdb_post_val):
    $cf7_id = get_post_meta($cf7stdb_post_key, 'cf7stdb_cf7_id', true);
    array_push($entries_array, $cf7_id);
endforeach;
$cf7_post_array = array();
foreach ($cf7_posts as $cf7stdb_key => $cf7stdb_val):
    array_push($cf7_post_array, $cf7stdb_key);
endforeach;
$file_header_orginalstr = array("#current_timestamp");
$file_header_replacestr = array(time());
$filename = isset($cd7stdb_options['cd7stdb_entry_report_img_title']) && !empty($cd7stdb_options['cd7stdb_entry_report_img_title']) ? str_replace($file_header_orginalstr, $file_header_replacestr, esc_attr($cd7stdb_options['cd7stdb_entry_report_img_title'])) : 'cf7stdb-entry-report';
?>

<div class="cd7stdb-wrapper cd7stdb-clear">
    <div class="cd7stdb-head">     
        <?php include(CF7STDB_PATH . 'includes/view/header.php'); ?> 
    </div> 
    <div class="cd7stdb-inner-wrapper" id="poststuff">
        <div id="post-body-full" class="metabox-holder columns-2">
            <div id="post-body-content">
                <div class="postbox">
                    <div class="cd7stdb-menu-option-wrapper clearfix" id="col-container">
                        <div class="inside" id="cd7stdb-reports-setting-wrapper">
                            <div class="cd7stdb-header-title cd7stdb-menu-option-header-title">
                                <h3><?php _e('Entries Report', CF7STDB_TXT_DOMAIN); ?></h3>
                            </div>
                            <form action="<?php echo admin_url() . 'admin-post.php' ?>" method='post' id="cd7stdb-menu-option-form">     
                                <input type="hidden" name="action" value="cd7stdb_generate_cd7stdb_report_options" />
                                <div class="cd7stdb-entry-report-filter-wrap">
                                    <div class="cd7stdb-option-inner-input">
                                        <label>
                                            <?php _e('Select Contact Form', CF7STDB_TXT_DOMAIN); ?>:
                                        </label>
                                        <select id="entries-post-form-specific" name="entries_post_form_specific">
                                            <option value=""><?php _e('All', CF7STDB_TXT_DOMAIN); ?></option>
                                            <?php
                                            if (isset($cf7_posts) && !empty($cf7_posts)) {
                                                foreach ($cf7_posts as $cf7_posts_key => $cf7_posts_val):
                                                    ?>
                                                    <option value="<?php echo $cf7_posts_key; ?>" <?php echo isset($cf7_form_id) && $cf7_form_id == $cf7_posts_key ? 'selected="selected"' : ''; ?>><?php echo $cf7_posts_val; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="cd7stdb-option-inner-input">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <label>
                                            <?php _e('Select Year', CF7STDB_TXT_DOMAIN); ?>:
                                        </label>
                                        <select id="entries-post-form-specific" name="entries_year_specific">
                                            <option value="2018" <?php echo isset($entries_year_specific) && $entries_year_specific == 2018 ? 'selected="selected"' : ''; ?>><?php _e('2018', CF7STDB_TXT_DOMAIN); ?></option>
                                            <option value="2019" <?php echo isset($entries_year_specific) && $entries_year_specific == 2019 ? 'selected="selected"' : ''; ?>><?php _e('2019', CF7STDB_TXT_DOMAIN); ?></option>
                                        </select>
                                    </div>
                                    <div class="cd7stdb-option-inner-input">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <label>
                                            <?php _e('Select Month', CF7STDB_TXT_DOMAIN); ?>:
                                        </label>
                                        <select id="entries-post-form-specific" name="entries_month_specific">
                                            <option value="">
                                                <?php _e('All', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="1" <?php echo isset($entries_month_specific) && $entries_month_specific == '1' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('January', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="2" <?php echo isset($entries_month_specific) && $entries_month_specific == '2' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('February', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="3" <?php echo isset($entries_month_specific) && $entries_month_specific == '3' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('March', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="4" <?php echo isset($entries_month_specific) && $entries_month_specific == '4' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('April', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="5" <?php echo isset($entries_month_specific) && $entries_month_specific == '5' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('May', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="6" <?php echo isset($entries_month_specific) && $entries_month_specific == '6' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('June', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="7" <?php echo isset($entries_month_specific) && $entries_month_specific == '7' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('July', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="8" <?php echo isset($entries_month_specific) && $entries_month_specific == '8' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('August', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="9" <?php echo isset($entries_month_specific) && $entries_month_specific == '9' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('September', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="10" <?php echo isset($entries_month_specific) && $entries_month_specific == '10' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('Ocotber', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="11" <?php echo isset($entries_month_specific) && $entries_month_specific == '11' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('November', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                            <option value="12" <?php echo isset($entries_month_specific) && $entries_month_specific == '12' ? 'selected="selected"' : ''; ?>>
                                                <?php _e('December', CF7STDB_TXT_DOMAIN); ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <?php wp_nonce_field('cd7stdb_generate_cd7stdb_rep_nonce', 'cd7stdb_generate_cd7stdb_rep_add_nonce'); ?>
                                <div class="cd7stdb-submit-wrap">
                                    <input type="submit" class="button-primary" id="cd7stdb-generate-report-button" name='cd7stdb_generate_report_button' value="<?php _e('Generate Report', CF7STDB_TXT_DOMAIN); ?>" />                                   
                                    <input type="button" class="button-primary" id="cd7stdb-store-report-button" name='cd7stdb_save_report_button' value="<?php _e('Save Report as Image', CF7STDB_TXT_DOMAIN); ?>" />
                                    <span class="spinner cf7stdb-report-load-wrap is-active" style="display:none;"></span>
                                </div>
                                <?php
                                $form_data = array();
                                $form_title_data = array();
                                $month_arrays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
                                $date_arrays_31 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
                                $date_arrays_30 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
                                $date_arrays_28 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
                                /* If both contact form 7 id and Month isn't empty */
                                if (isset($_GET['action']) && $_GET['action'] == 'indiv_report' && isset($_GET['cf7_form_id']) && !empty($_GET['cf7_form_id']) && isset($_GET['cf7_month']) && !empty($_GET['cf7_month'])) {
                                    $date_month_flag = 'date';
                                    if (in_array($entries_month_specific, array(1, 3, 5, 7, 8, 10, 12))):
                                        $date_arrays = $date_arrays_31;
                                    elseif (in_array($entries_month_specific, array(4, 6, 9, 11))):
                                        $date_arrays = $date_arrays_30;
                                    else:
                                        $date_arrays = $date_arrays_28;
                                    endif;
                                    foreach ($date_arrays as $date_array):
                                        $args = array(
                                            'post_type' => 'cf7storetodbs',
                                            'meta_query' => array(
                                                'relation' => 'OR',
                                                array(
                                                    'key' => 'cf7stdb_cf7_id',
                                                    'value' => $cf7_form_id,
                                                    'compare' => '='
                                                )
                                            ),
                                            'date_query' => array(
                                                array(
                                                    'year' => $entries_year_specific,
                                                    'monthnum' => $entries_month_specific,
                                                    'day' => $date_array
                                                ),
                                            )
                                        );

                                        $query = new WP_Query($args);
                                        $cf7_title = get_the_title($_GET['cf7_form_id']);
                                        array_push($form_title_data, $cf7_title);
                                        $count = $query->post_count;
                                        array_push($form_data, $count);
                                        wp_reset_postdata();
                                    endforeach;
                                    $datasets[] = [
                                        'label' => $cf7_title,
                                        'borderColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 1)',
                                        'backgroundColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 0.3)',
                                        'borderWidth' => 1,
                                        'data' => $form_data
                                    ];
                                    /* If both contact form 7 id and Month isn't empty ends */
                                } else if (isset($_GET['action']) && $_GET['action'] == 'indiv_report' && isset($_GET['cf7_form_id']) && !empty($_GET['cf7_form_id']) && isset($_GET['cf7_month']) && empty($_GET['cf7_month'])) {
                                    /* If contact form 7 id isn't empty and Month is empty */
                                    $date_month_flag = 'month';
                                    foreach ($month_arrays as $month_array):
                                        $args = array(
                                            'post_type' => 'cf7storetodbs',
                                            'meta_query' => array(
                                                'relation' => 'OR',
                                                array(
                                                    'key' => 'cf7stdb_cf7_id',
                                                    'value' => $cf7_form_id,
                                                    'compare' => '='
                                                )
                                            ),
                                            'date_query' => array(
                                                array(
                                                    'year' => $entries_year_specific,
                                                    'monthnum' => $month_array
                                                ),
                                            )
                                        );

                                        $query = new WP_Query($args);
                                        $cf7_title = get_the_title($_GET['cf7_form_id']);
                                        array_push($form_title_data, $cf7_title);
                                        $count = $query->post_count;
                                        array_push($form_data, $count);
                                        wp_reset_postdata();
                                    endforeach;
                                    $datasets[] = [
                                        'label' => $cf7_title,
                                        'borderColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 1)',
                                        'backgroundColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 0.3)',
                                        'borderWidth' => 1,
                                        'data' => $form_data
                                    ];
                                    /* If contact form 7 id isn't empty and Month is empty ends */
                                } else if (isset($_GET['action']) && $_GET['action'] == 'indiv_report' && isset($_GET['cf7_form_id']) && empty($_GET['cf7_form_id']) && isset($_GET['cf7_month']) && !empty($_GET['cf7_month'])) {
                                    /* If contact form 7 id is empty and Month isn't empty */
                                    $date_month_flag = 'date';
                                    if (in_array($entries_month_specific, array(1, 3, 5, 7, 8, 10, 12))):
                                        $date_arrays = $date_arrays_31;
                                    elseif (in_array($entries_month_specific, array(4, 6, 9, 11))):
                                        $date_arrays = $date_arrays_30;
                                    else:
                                        $date_arrays = $date_arrays_28;
                                    endif;
                                    if (!empty($cf7_posts)) {
                                        foreach ($cf7_posts as $cf7_posts_key => $cf7_posts_val):
                                            foreach ($date_arrays as $date_array_val):
                                                $args = array(
                                                    'post_type' => 'cf7storetodbs',
                                                    'order' => 'ASC',
                                                    'meta_query' => array(
                                                        'relation' => 'OR',
                                                        array(
                                                            'key' => 'cf7stdb_cf7_id',
                                                            'value' => $cf7_posts_key,
                                                            'compare' => '='
                                                        )
                                                    ),
                                                    'date_query' => array(
                                                        array(
                                                            'year' => $entries_year_specific,
                                                            'month' => $entries_month_specific,
                                                            'day' => $date_array_val
                                                        ),
                                                    )
                                                );
                                                $query = new WP_Query($args);
                                                $cf7_title = get_the_title($cf7_posts_key);
                                                array_push($form_title_data, $cf7_title);
                                                $count = $query->post_count;
                                                array_push($form_data, $count);
                                                wp_reset_postdata();
                                            endforeach;
                                        endforeach;
                                    }
                                    $form_title_data_chunk = array_chunk($form_title_data, 31);
                                    $form_data_chunk = array_chunk($form_data, 31);
                                    foreach ($form_data_chunk as $form_data_chunky => $form_data_chunki):
                                        foreach ($form_title_data_chunk as $form_title_data_chunky => $form_title_data_chunki):
                                            if ($form_title_data_chunky == $form_data_chunky):
                                                $datasets[] = [
                                                    'label' => $form_title_data_chunki[0],
                                                    'borderColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 1)',
                                                    'backgroundColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 0.3)',
                                                    'borderWidth' => 1,
                                                    'data' => $form_data_chunki
                                                ];
                                            endif;
                                        endforeach;
                                    endforeach;
                                    /* If contact form 7 id is empty and Month isn't empty ends */
                                } else {
                                    $date_month_flag = 'month';
                                    /* Else or Default Condition */
                                    if (!empty($cf7_posts)) {
                                        foreach ($cf7_posts as $cf7_posts_key => $cf7_posts_val):
                                            foreach ($month_arrays as $month_array_val):
                                                $args = array(
                                                    'post_type' => 'cf7storetodbs',
                                                    'order' => 'ASC',
                                                    'meta_query' => array(
                                                        'relation' => 'OR',
                                                        array(
                                                            'key' => 'cf7stdb_cf7_id',
                                                            'value' => $cf7_posts_key,
                                                            'compare' => '='
                                                        )
                                                    ),
                                                    'date_query' => array(
                                                        array(
                                                            'year' => $entries_year_specific,
                                                            'month' => $month_array_val
                                                        ),
                                                    )
                                                );
                                                $query = new WP_Query($args);
                                                $cf7_title = get_the_title($cf7_posts_key);
                                                array_push($form_title_data, $cf7_title);
                                                $count = $query->post_count;
                                                array_push($form_data, $count);
                                                wp_reset_postdata();
                                            endforeach;
                                        endforeach;
                                    }
                                    $form_title_data_chunk = array_chunk($form_title_data, 12);
                                    $form_data_chunk = array_chunk($form_data, 12);
                                    foreach ($form_data_chunk as $form_data_chunky => $form_data_chunki):
                                        foreach ($form_title_data_chunk as $form_title_data_chunky => $form_title_data_chunki):
                                            if ($form_title_data_chunky == $form_data_chunky):
                                                $datasets[] = [
                                                    'label' => $form_title_data_chunki[0],
                                                    'fill' => false,
                                                    'borderColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 1)',
                                                    'borderWidth' => 1.3,
                                                    'data' => $form_data_chunki
                                                ];
                                            endif;
                                        endforeach;
                                    endforeach;
                                }/* Else or Default Condition Ends */

                                $datasets = json_encode($datasets);
                                ?>
                                <div class="cd7stdb-report-wrap">
                                    <canvas 
                                        id="chart_0" 
                                        height="200" 
                                        width="600"
                                        data-flag-value ="<?php echo $date_month_flag; ?>"
                                        data-total-submission='<?php echo $datasets; ?>' 
                                        data-label-text="<?php _e('No. of Entries', CF7STDB_TXT_DOMAIN); ?>"
                                        data-label-header="<?php _e('Report On Contact Form 7 Entries', CF7STDB_TXT_DOMAIN); ?>"
                                        data-entry-img-label="<?php echo $filename; ?>"
                                        ></canvas>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 'backgroundColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 0.3)', -->