<?php
defined('ABSPATH') or die('No script kiddies please!');
?>

<?php
$cf7_posts = $this->cf7stdb_contact_form_7_plugin_posts();
$cd7stdb_options = get_option('cf7stdb_settings');
?>

<div class="cd7stdb-wrapper cd7stdb-clear">
    <div class="cd7stdb-head">     
        <?php include(CF7STDB_PATH . 'includes/view/header.php'); ?> 
    </div>
    <?php
    if (isset($_GET['message']) && $_GET['message'] == '3') {
        ?>
        <div class="cf7stdb-admin-notice notice notice-success is-dismissible">
            <p><?php _e('Setting Successully Updated.', CF7STDB_TXT_DOMAIN); ?></p>
        </div>
    <?php } else if (isset($_GET['message']) && $_GET['message'] == '4') {
        ?>
        <div class="cf7stdb-admin-notice notice notice-error is-dismissible">
            <p><?php _e('Setting Wasn\'t Updated. Please Try Again.', CF7STDB_TXT_DOMAIN); ?></p>
        </div>           
    <?php }
    ?>
    <div class = "cd7stdb-inner-wrapper" id = "poststuff">
        <div id = "post-body-full" class = "metabox-holder columns-2">
            <div id = "post-body-content">
                <div class = "postbox">
                    <div class = "cd7stdb-menu-option-wrapper clearfix" id = "col-container">
                        <div class = "inside" id = "cd7stdb-menu-setting-wrapper">
                            <div class = "cd7stdb-header-title cd7stdb-menu-option-header-title">
                                <h3><?php _e('General Settings', CF7STDB_TXT_DOMAIN); ?></h3>
                            </div>
                            <form action="<?php echo admin_url() . 'admin-post.php' ?>" method='post' id="cd7stdb-menu-option-form">     
                                <input type="hidden" name="action" value="cd7stdb_save_cd7stdb_options" />
                                <div class="cd7stdb-general-setting-field-wrap">
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Enable Storage', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable']) && $cd7stdb_options['cd7stdb_enable_disable'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Enable/Disable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Disable Attachment Storage', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable_attachment]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable_attachment']) && $cd7stdb_options['cd7stdb_enable_disable_attachment'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Disable/Enable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('When checked, no attachment will be uploaded.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Delete Attachment While Deleting Entries', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable_attach_deletion]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable_attach_deletion']) && $cd7stdb_options['cd7stdb_enable_disable_attach_deletion'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Disable/Enable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('When checked, attachment related with the specific post will also be deleted. Only works while empting trash', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Temporary Disable Storing For Specific Form', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <ul class="cd7stdb-post-specific-entry">
                                                <?php
                                                if (isset($cf7_posts) && !empty($cf7_posts)) {
                                                    foreach ($cf7_posts as $cf7_posts_key => $cf7_posts_val):
                                                        ?>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" id="cd7stdb-post-specific-entry-checkbox" class="cd7stdb-display-add-check" name="cd7stdb_general_setting[cd7stdb_post_specific_entry][]" id="cd7stdb-add-check-page" value="<?php echo $cf7_posts_key; ?>" <?php if (!empty($cd7stdb_options['cd7stdb_post_specific_entry']) && in_array($cf7_posts_key, $cd7stdb_options['cd7stdb_post_specific_entry'])) { ?>checked="checked"<?php } ?>/>
                                                                <span class="pages"><?php echo $cf7_posts_val; ?></span>
                                                            </label>
                                                        </li>
                                                        <?php
                                                    endforeach;
                                                } else {
                                                    ?>
                                                    <p>
                                                        <?php _e('No Contact Form at the moment. Please Try adding One.', CF7STDB_TXT_DOMAIN); ?>
                                                    </p>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Individual CSV File Name', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="text" placeholder="cf7stdb-ind-entry-#current_timestamp" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_ind_csv_title]" value="<?php
                                                if (isset($cd7stdb_options['cd7stdb_ind_csv_title']) && !empty($cd7stdb_options['cd7stdb_ind_csv_title'])) {
                                                    echo esc_attr($cd7stdb_options['cd7stdb_ind_csv_title']);
                                                }
                                                ?>"/>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If left empty default title will be set as "cf7stdb-ind-entry-#current_timestamp". You can use #current_timestamp for current time value.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Group CSV File Name', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="text" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_group_csv_title]" value="<?php
                                                if (isset($cd7stdb_options['cd7stdb_group_csv_title']) && !empty($cd7stdb_options['cd7stdb_group_csv_title'])) {
                                                    echo esc_attr($cd7stdb_options['cd7stdb_group_csv_title']);
                                                }
                                                ?>"/>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If left empty default title will be set as "cf7stdb-group-entries-#current_timestamp". You can use #current_timestamp for current time value.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Entry Report Image File Name', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="text" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_entry_report_img_title]" value="<?php
                                                if (isset($cd7stdb_options['cd7stdb_entry_report_img_title']) && !empty($cd7stdb_options['cd7stdb_entry_report_img_title'])) {
                                                    echo esc_attr($cd7stdb_options['cd7stdb_entry_report_img_title']);
                                                }
                                                ?>"/>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If left empty default title will be set as "cf7stdb-entry-report". You can use #current_timestamp for current time value.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Enable Direct Delete of Entries Post', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable_skip_trash]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable_skip_trash']) && $cd7stdb_options['cd7stdb_enable_disable_skip_trash'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Enable/Disable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If checked, trashing the entries will skip trash and permanently delete the entries', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <h3><?php _e('GDPR Consent Option', CF7STDB_TXT_DOMAIN); ?></h3>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Disable Storing of the IP Address and Browser Data', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable_disable_device_data]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable_disable_device_data']) && $cd7stdb_options['cd7stdb_enable_disable_disable_device_data'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Disable/Enable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If checked, user data such as public IP address and browser detail won\'t be stored, for GDPR reason', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Enable/Disable GDPR Consent On the All Forms', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <input type="checkbox" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_enable_disable_disable_consent]" <?php if (isset($cd7stdb_options['cd7stdb_enable_disable_disable_consent']) && $cd7stdb_options['cd7stdb_enable_disable_disable_consent'] == 'on') { ?>checked="checked"<?php } ?>/>
                                                <span class="cd7stdb-check-text"><?php _e('Enable/Disable', CF7STDB_TXT_DOMAIN); ?></span>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('If checked, checkbox will be added to the form with GDPR consent.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Consent Checkbox Label', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <label>
                                                <textarea class="cd7stdb-textarea" name="cd7stdb_general_setting[cd7stdb_gdpr_consent_checkbox_label]" cols="38" rows="6"><?php echo isset($cd7stdb_options['cd7stdb_gdpr_consent_checkbox_label']) && !empty($cd7stdb_options['cd7stdb_enable_disable_disable_consent']) ? esc_attr($cd7stdb_options['cd7stdb_gdpr_consent_checkbox_label']) : ''; ?></textarea>
                                            </label>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('Default label: "I am okay with storage of my data and accept Privacy Policy". Please Use "#PrivacyPolicy" for linking privacy policy page.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Privacy Policy Link Text', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <input type="text" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_gdpr_policy_link_label]" value="<?php
                                            if (isset($cd7stdb_options['cd7stdb_gdpr_policy_link_label']) && !empty($cd7stdb_options['cd7stdb_gdpr_policy_link_label'])) {
                                                echo esc_attr($cd7stdb_options['cd7stdb_gdpr_policy_link_label']);
                                            }
                                            ?>"/>
                                        </div>
                                        <p class="cd7stdb-description"><?php _e('Default text is "Privacy Policy" for a #PrivacyPolicy link generated.', CF7STDB_TXT_DOMAIN); ?></p>
                                    </div>
                                    <div class="cd7stdb-input-field-wrap">
                                        <label>
                                            <h4><?php _e('Privacy Policy URL', CF7STDB_TXT_DOMAIN); ?>:</h4>
                                        </label>
                                        <div class="cd7stdb-input-field">
                                            <input type="url" class="cd7stdb-checkbox" name="cd7stdb_general_setting[cd7stdb_gdpr_policy_page_url]" value="<?php
                                            if (isset($cd7stdb_options['cd7stdb_gdpr_policy_page_url']) && !empty($cd7stdb_options['cd7stdb_gdpr_policy_page_url'])) {
                                                echo esc_url($cd7stdb_options['cd7stdb_gdpr_policy_page_url']);
                                            }
                                            ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <?php wp_nonce_field('cd7stdb_nonce_save_post_specific_storage_settings', 'cd7stdb_add_nonce_save_post_specific_storage_settings'); ?>
                                <div class="cd7stdb-submit-wrap">
                                    <input type="submit" class="button-primary" name='cd7stdb_save_cd7stdb_settings' value="<?php _e('Save Settings', CF7STDB_TXT_DOMAIN); ?>" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>