jQuery(document).ready(function ($) {

    $('.et_pb_contact_field .wpcf7-form-control').click(function () {
        if (!$('.et_pb_contact_field .wpcf7-form-control').hasClass('clicked')) {
            $('.et_pb_contact_field .wpcf7-form-control').addClass('clicked');
            $('.et_pb_contact_field .wpcf7-form-control').blur();
            $('.politiki-popup').fadeIn(400);
        }
    });
    $('.tnp-field input').click(function () {
        if (!$('.tnp-field input').hasClass('clicked')) {
            $('.tnp-field input').addClass('clicked');
            $('.tnp-field input').blur();
            $('.politiki-popup--news').fadeIn(400);
        }
    });

    $('.politiki-popup__btn').click(function () {
        $('.politiki-popup').fadeOut(400);
    });

    $('.forms-popup__btn').click(function () {
        $('.forms-popup').fadeOut(400);
    });

    $('.politiki-popup__btn--news').click(function () {
        $('.politiki-popup--news').fadeOut(400);
    });

    $('#agree-1').change(function () {
        $('#check-1').toggleClass('check-1');
    });

    var education_calc = 1;
    var language_calc = 1;
    var rec_calc = 1;
    var exp_calc = 1;
    $("#add_education_history").click(function (e) {
        e.preventDefault();
        var tupos = $('span.education_type').html();
        var idruma = $('span.education_institution').html();
        var titlos = $('span.education_title').html();
        var imerominia_enarksis = $('span.education_start_date').html();
        var etos_apof = $('span.education_finish_year').html();
        var edu_type_options = $('#edu_history_type option');
        var edu_type_values = $.map(edu_type_options, function(option) {
            return option.outerHTML;
        });
        $('<div class="clone_edu new">'+
            '<div><p><label>'+tupos+'<br><select class="wpcf7-form-control" name="Details[EducationHistory][' + education_calc + '][Type]">'+edu_type_values+'</select></label></p>' +
            '<p><label>'+idruma+'<br><input type="text" class="wpcf7-form-control" name="Details[EducationHistory][' + education_calc + '][Institution]" value="" /></p>' +
            '<p><label>'+titlos+'<br><input type="text" class="wpcf7-form-control" name="Details[EducationHistory][' + education_calc + '][Title]" value="" /></p></div>' +
            '<div><p><label>'+imerominia_enarksis+'<br><input type="number" class="wpcf7-form-control" name="Details[EducationHistory][' + education_calc + '][StartDate]" value="" /></p>' +
            '<p><label>'+etos_apof+'<br><input type="number" class="wpcf7-form-control" name="Details[EducationHistory][' + education_calc + '][GraduationDate]" value="" /></p></div>' +
            '</div>').insertAfter('.clone-edu:last');
        education_calc++;
        return false;
    });

    $("#add_exp").click(function (e) {
        e.preventDefault();
        var previous_exp_txt = $('span.previous_exp_txt').html();
        var previous_exp_city = $('span.previous_exp_city').html();
        var previous_exp_country = $('span.previous_exp_country').html();
        var previous_exp_tmima = $('span.previous_exp_tmima').html();
        var previous_exp_pos = $('span.previous_exp_pos').html();
        var previous_exp_from = $('span.previous_exp_from').html();
        var previous_exp_to = $('span.previous_exp_to').html();
        var previous_countr = $('span.prev_exp_country>select option');
        var previous_countries_opts = $.map(previous_countr, function(option) {
            return option.outerHTML;
        });

        $('<div class="clone_exp new">' +
            '<div>' +
            '<p><label><span class="previous_exp_txt">'+previous_exp_txt+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_descr"><input type="text" name="Details[PreviousEmployment]['+exp_calc+'][EmployerDescr]" value="" size="40" class="wpcf7-form-control wpcf7-text " aria-required="true" aria-invalid="false"></span></label></p>' +
            '<p><label><span class="previous_exp_city">'+previous_exp_city+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_city"><input type="text" name="Details[PreviousEmployment]['+exp_calc+'][EmployerCityDescr]" value="" size="40" class="wpcf7-form-control wpcf7-text " aria-required="true" aria-invalid="false"></span></label></p>' +
            '<p><label><span class="previous_exp_country">'+previous_exp_country+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_country"><select name="Details[PreviousEmployment]['+exp_calc+'][EmployerCountryID]" class="wpcf7-form-control wpcf7-select " aria-required="true" aria-invalid="false">'+previous_countries_opts+'</select></span></label></p>' +
            '</div>' +
            '<div>' +
            '<p><label><span class="previous_exp_tmima">'+previous_exp_tmima+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_tmima"><input type="text" name="Details[PreviousEmployment]['+exp_calc+'][DepartmentDescr]" value="" size="40" class="wpcf7-form-control wpcf7-text " aria-required="true" aria-invalid="false"></span></label></p>' +
            '<p><label><span class="previous_exp_pos">'+previous_exp_pos+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_pos"><input type="text" name="Details[PreviousEmployment]['+exp_calc+'][PositionDescr]" value="" size="40" class="wpcf7-form-control wpcf7-text " aria-required="true" aria-invalid="false"></span></label></p>' +
            '<p><label><span class="previous_exp_from">'+previous_exp_from+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_from"><input type="date" name="Details[PreviousEmployment]['+exp_calc+'][FromDate]" value="" class="wpcf7-form-control wpcf7-date " aria-required="true" aria-invalid="false"></span></label></p>' +
            '</div>' +
            '<div>' +
            '<p><label><span class="previous_exp_to">'+previous_exp_to+'</span><br>' +
            '<span class="wpcf7-form-control-wrap prev_exp_to"><input type="date" name="Details[PreviousEmployment]['+exp_calc+'][ToDate]" value="" class="wpcf7-form-control wpcf7-date " aria-required="true" aria-invalid="false"></span></label></p>' +
            '</div>' +
            '</div>').insertAfter('.clone_exp:last');
        exp_calc++;
        return false;
    });

    $("#add_languages_group").click(function (e) {
        e.preventDefault();
        var lang = $('span.edu_lang').html();
        var edu_lang_read = $('span.edu_lang_read').html();
        var lang_options = $('select#langs > option');
        var languages = $.map(lang_options, function(option) {
            return option.outerHTML;
        });
        var lang_levels = $('#lang_levels option');
        var lang_leve_opts = $.map(lang_levels, function(option) {
            return option.outerHTML;
        });
        $('<div class="clone_lang new"><div><p><label><span class="edu_lang">'+lang+'</span><br/><select class="wpcf7-form-control" name="Details[Languages]['+language_calc+'][LangId]">'+languages+'</select></label></p>'+
        '<p><label><span class="edu_lang_read"> '+edu_lang_read+'</span><br/><select class="wpcf7-form-control" name="Details[Languages]['+language_calc+'][LangLevelRead]">'+lang_leve_opts+'</select></label></p>'+
        '</div>').insertAfter('.clone_lang:last');
        language_calc++;
        return false;
    });

    $("#add_recommendations").click(function (e) {
        e.preventDefault();
        var recommendation_rec = $('span.recommendation_rec').html();
        var recommendation_pos = $('span.recommendation_pos').html();
        var recommendation_comp = $('span.recommendation_comp').html();
        var recommendation_date = $('span.recommendation_date').html();
        
        $('<div class="clone_rec new"><div><p><label><span class="recommendation_rec">'+recommendation_rec+'</span><br/><input type="text" class="wpcf7-form-control" name="Details[Recommendations]['+rec_calc+'][Recommender]" value="" /></label></p>' +
            '<p><label><span class=recommendation_pos">'+recommendation_pos+'</span><br/><input type="text" class="wpcf7-form-control" name="Details[Recommendations]['+rec_calc+'][Position]" value="" /></label></p>' +
            '<p><label><span class="recommendation_comp">'+recommendation_comp+'</span><br/><input type="text" class="wpcf7-form-control" name="Details[Recommendations]['+rec_calc+'][Company]" value=""  /></label></p></div>' +
            '<p><label><span class="recommendation_date">'+recommendation_date+'</span><br/><input type="date" class="wpcf7-form-control" name="Details[Recommendations]['+rec_calc+'][RecommendationDate]" value="" /></label></p></div>'+
        '</div>').insertAfter('.clone_rec:last');
        rec_calc++;
        return false;
    });
    
    $("#delete_education_history").click(function (e) {
        e.preventDefault();
        if (education_calc > 1) {
            $('.clone_edu:last').remove();
            education_calc--;
        }
    });

    $("#delete_languages_group").click(function (e) {
        e.preventDefault();
        if (language_calc > 1) {
            $('.clone_lang:last').remove();
            language_calc--;
        }
    });

    $("#delete_exp").click(function (e) {
        e.preventDefault();
        if (exp_calc > 1) {
            $('.clone_exp:last').remove();
            exp_calc--;
        }
    });

    $("#delete_recommendations").click(function (e) {
        e.preventDefault();
        if (rec_calc > 1) {
            $('.clone_rec:last').remove();
            rec_calc--;
        }
    });
});