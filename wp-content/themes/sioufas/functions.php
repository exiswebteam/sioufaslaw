<?php
add_action('wp_enqueue_scripts', 'divi_parent_theme_enqueue_styles');
function divi_parent_theme_enqueue_styles() {
	wp_enqueue_style('divi-style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('sioufas-style',
		get_stylesheet_directory_uri() . '/style.css',
		array('divi-style')
	);
	wp_enqueue_script('divi-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), false, false);
//	wp_enqueue_script('my-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), false, true);
}

/**
 * New Widget Areas
 */

function sioufas_widget_init() {
	register_sidebar(array(
		'name' => 'Law Sector Sidebar',
		'id' => 'law_sector_sidebar',
		'before_widget' => '<div class="law-sector-sidebar">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Top Line Right',
		'id' => 'top_line_right',
		'before_widget' => '<div class="top-line-right">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Top Right Sidebar',
		'id' => 'top_right_1',
		'before_widget' => '<div class="top-right-wa">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Footer Top - Horizontal',
		'id' => 'footer_top_100',
		'before_widget' => '<div class="footer-top-wa footer-widgets">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Footer Mid - Left',
		'id' => 'footer_mid_50_1',
		'before_widget' => '<div class="footer-mid-wa-1 footer-widgets footer-mid-widgets">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Footer Mid - Right',
		'id' => 'footer_mid_50_2',
		'before_widget' => '<div class="footer-mid-wa-1 footer-widgets footer-mid-widgets">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Footer Bottom - Horizontal',
		'id' => 'footer_bot_100',
		'before_widget' => '<div class="footer-bot-wa footer-widgets">',
		'after_widget' => '</div>',
	));
}

add_action('widgets_init', 'sioufas_widget_init');

/**
 * End of Widget Areas
 */

add_filter('locale', 'wpse27056_setLocale');
function wpse27056_setLocale($locale) {
	if (is_admin()) {
		return 'en_US';
	}
	return $locale;
}

function job_post_type() {
	$args = [
		'label' => __('Jobs', 'sioufas'),
		'description' => __('Jobs post type', 'sioufas'),
		'supports' => ['title', 'editor', 'excerpt', 'author', 'thumbnail'],
		'menu_position' => 4,
		'public' => true,
		'has_archive' => true,
		'menu_icon' => 'dashicons-businessman',
		'capability_type' => 'page',
	];
	register_post_type('job', $args);
}

add_action('init', 'job_post_type');

function job_taxononomies() {
	$taxonomies = array(
		'name' => _x('Job Tags', 'taxonomy general name'),
		'singular_name' => _x('Job Tag', 'taxonomy singular name'),
		'search_items' => __('Search Job Tags'),
		'popular_items' => __('Popular Job Tags'),
		'all_items' => __('All Job Tags'),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __('Edit Job Tag'),
		'update_item' => __('Update Job Tag'),
		'add_new_item' => __('Add New Tag'),
		'new_item_name' => __('New Tag Name'),
		'separate_items_with_commas' => __('Separate Job tags with commas'),
		'add_or_remove_items' => __('Add or remove Job tags'),
		'choose_from_most_used' => __('Choose from the most used Job tags'),
		'menu_name' => __('Job Tags'),
	);
	register_taxonomy('jobs_tags', 'job', [
		'hierarchical' => false,
		'labels' => $taxonomies,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => ['slug' => 'jobs_tags'],
	]);
}

add_action('init', 'job_taxononomies');

if (function_exists('acf_add_options_page')) {
	$options = [
		'page_title' => __('Theme Options', 'acf'),
		'menu_title' => __('Theme Options', 'acf'),
		'menu_slug' => __('theme-options', 'acf'),
		'icon_url' => 'dashicons-admin-tools',
	];
	acf_add_options_page($options);
}


add_filter('wpcf7_form_elements', 'do_shortcode');

add_filter('https_local_ssl_verify', '__return_false');
