<form action="<?= $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data" id="submit_cv_form" method="POST"
      class="wpcf7-form <?= $success ? 'd-none' : null; ?>" novalidate>
	<?php $jobs_sql = "SELECT * FROM `position`";
	if ($result = $mysqli->query($jobs_sql)) { ?>
        <div class="jobs_checkboxes text-center">
            <h2 class="text-center"><?php esc_html_e('Θέσεις εργασίας'); ?></h2>
            <h6 class="text-center"><?php esc_html_e('(Μπορείτε να επιλέξετε πάνω από 1 θέση εργασίας)', 'exis'); ?></h6>
			<?php while ($job = $result->fetch_assoc()) { ?>
                <div class="text-center">
                    <label>
                        <input type="checkbox" <?= (mysqli_real_escape_string($mysqli, trim($_GET['position'])) == $job['ID_POS']) ? 'checked' : ''; ?>
                               name="Details[Positions][][ID_POS]" value="<?= $job['ID_POS']; ?>"/>
                        <span class="text-center position_description"><?= $job['DESCR']; ?></span>
                    </label>
                </div>
			<?php }
			$result->free(); ?>
        </div>
	<?php } ?>
    <input type="hidden" name="ExternalCVID" value="" class="wpcf7-form-control wpcf7-hidden">
    <div class="personal_div">
        <h2 class="text-center"><?= esc_html__("Προσωπικα στοιχεια", 'exis'); ?></h2>
        <p>
            <label><?= esc_html__("Όνομα*", 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap FirstName">
                        <input type="text" name="FirstName"
                               value="<?= $_POST['FirstName'] ? $_POST['FirstName'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['FirstName'] ? 'error_border' : ''; ?>"
                               required/>
                    <span class="error <?= $errors_arr['FirstName'] ? 'visible' : ''; ?>"><?= $error ?></span>
                </span>
            </label>
        </p>
        <p>
            <label> <?= esc_html__('Επώνυμο*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap LastName">
                        <input type="text" name="LastName" value="<?= $_POST['LastName'] ? $_POST['LastName'] : '' ?>"
                               size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['LastName'] ? 'error_border' : ''; ?>"/>
                            <span class="error <?= $errors_arr['LastName'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__("Όνομα πατρός*", 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap FathersName">
                        <input type="text" name="FathersName"
                               value="<?= $_POST['FathersName'] ? $_POST['FathersName'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['FathersName'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['FathersName'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label> <?= esc_html__("Ημερομηνία γέννησης*", "exis"); ?><br>
                <span class="wpcf7-form-control-wrap BirthDate">
                        <input type="date" name="BirthDate" min="1900-01-01" max="<?= date('Y-m-d'); ?>"
                               value="<?= $_POST['BirthDate'] ? $_POST['BirthDate'] : '' ?>"
                               class="wpcf7-form-control wpcf7-date <?= $errors_arr['BirthDate'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['BirthDate'] ? 'visible' : ''; ?>"><?= $errors_arr['BirthDate'] ?: $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label> <?= esc_html__("Οικογενειακή κατάσταση*", 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap MaritalType">
                        <select name="MaritalType"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['MaritalType'] ? 'error_border' : ''; ?>">
                            <option value="">---</option>
                            <?php $marital = [0 => 'Έγγαμος/η', 1 => 'Άγαμος/η', 2 => 'Διαζευγμένος/η', 3 => 'Χήρος/α', 4 => 'Σύμφωνο Συμβίωσης'];
                            $marital_opts = '';
                            foreach ($marital as $key => $val) {
	                            $marital_opts .= '<option ';
	                            $marital_opts .= is_numeric($_POST['MaritalType']) && $_POST['MaritalType'] == $key ? ' selected ' : '';
	                            $marital_opts .= ' value="' . $key . '" >' . esc_html__($val, 'exis');
	                            $marital_opts .= '</option>';
                            };
                            echo $marital_opts; ?>
                        </select>
                        <span class="error <?= $errors_arr['MaritalType'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label> <?= esc_html__("Χώρα Διαμονής*", "exis"); ?><br>
                <span class="wpcf7-form-control-wrap CountryOfResidenceCode">
                        <select name="CountryOfResidenceCode"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['CountryOfResidenceCode'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php countries($_POST['CountryOfResidenceCode'], $mysqli); ?>
                        </select>
                        <span class="error <?= $errors_arr['CountryOfResidenceCode'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
    </div>
    <div class="contact_div">
        <h2 class="text-center"><?= esc_html__('Στοιχεια επικοινωνιας', 'exis'); ?></h2>
        <p>
            <label><?= esc_html__('e-mail*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap email">
                        <input type="email" name="email" value="<?= $_POST['email'] ? $_POST['email'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email <?= $errors_arr['email'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['email'] ? 'visible' : ''; ?>"><?= $errors_arr['email'] ?: $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Κινητό*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap Mobile">
                        <input type="text" name="Mobile" value="<?= $_POST['Mobile'] ? $_POST['Mobile'] : '' ?>"
                               size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['Mobile'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['Mobile'] ? 'visible' : ''; ?>"><?= $errors_arr['Mobile'] ? $errors_arr['Mobile'] : $error; ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Τηλέφωνο*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap Telephone">
                        <input type="text" name="Telephone"
                               value="<?= $_POST['Telephone'] ? $_POST['Telephone'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['Telephone'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['Telephone'] ? 'visible' : ''; ?>"><?= $errors_arr['Telephone'] ? $errors_arr['Telephone'] : $error; ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Διεύθυνση*', 'exis'); ?> <br>
                <span class="wpcf7-form-control-wrap AddressStreet">
                        <input type="text" name="AddressStreet"
                               value="<?= $_POST['AddressStreet'] ? $_POST['AddressStreet'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['AddressStreet'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['AddressStreet'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Αριθμός*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap AddressNumber">
                        <input type="text" name="AddressNumber"
                               value="<?= $_POST['AddressNumber'] ? $_POST['AddressNumber'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['AddressNumber'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['AddressNumber'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Πόλη*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap AddressCity">
                        <input type="text" name="AddressCity"
                               value="<?= $_POST['AddressCity'] ? $_POST['AddressCity'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['AddressCity'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['AddressCity'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Tαχυδρομικός Κώδικας*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap AddressZipCode">
                        <input type="number" name="AddressZipCode"
                               value="<?= $_POST['AddressZipCode'] ? $_POST['AddressZipCode'] : '' ?>" size="5"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['AddressZipCode'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['AddressZipCode'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Περιοχή*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap Area">
                        <input type="text" name="Area" value="<?= $_POST['Area'] ? $_POST['Area'] : '' ?>" size="40"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['Area'] ? 'error_border' : ''; ?>"
                               required/>
                            <span class="error <?= $errors_arr['Area'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
    </div>
    <div class="edu_div">
        <h2 class="text-center"><?= esc_html__('Εκπαιδευση και καταρτιση', 'exis'); ?></h2>
        <p>
            <label><?= esc_html__('Γνώση υπολογιστών*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap HasComputerKnowledge">
                        <select name="HasComputerKnowledge"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['HasComputerKnowledge'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php yes_no($_POST['HasComputerKnowledge']); ?>
                        </select>
                        <span class="error <?= $errors_arr['HasComputerKnowledge'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('ECDL*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap HasECDL">
                        <select name="HasECDL"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['HasECDL'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php yes_no($_POST['HasECDL']); ?>
                        </select>
                    </span>
                <span class="error <?= $errors_arr['HasECDL'] ? 'visible' : ''; ?>"><?= $error; ?></span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Βαθμός ECDL', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap ECDLRank">
                        <input type="number" name="ECDLRank" value="<?= $_POST['ECDLRank'] ? $_POST['ECDLRank'] : '' ?>"
                               max="99"
                               class="wpcf7-form-control wpcf7-text <?= $errors_arr['ECDLRank'] ? 'error_border' : ''; ?>"
                               placeholder="από 1 έως 99"/>
                    <span class="error <?= $errors_arr['ECDLRank'] ? 'visible' : null; ?>"><?= $errors_arr['ECDLRank'] ? $errors_arr['ECDLRank'] : 'Το πεδίο είναι υποχρεωτικό αν έχετε ECDL'; ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Γνώση Excel*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap MSEXCEL">
                        <select name="MSEXCEL"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['MSEXCEL'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php yes_no($_POST['MSEXCEL']); ?>
                        </select>
                        <span class="error <?= $errors_arr['MSEXCEL'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Γνώσεις PowerPoint*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap MSPOWERPOINT">
                        <select name="MSPOWERPOINT"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['MSPOWERPOINT'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php yes_no($_POST['MSPOWERPOINT']); ?>
                        </select>
                        <span class="error <?= $errors_arr['MSPOWERPOINT'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Γνώσεις MS Access*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap MSACCESS">
                        <select name="MSACCESS"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['MSACCESS'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?php yes_no($_POST['MSACCESS']); ?>
                        </select>
                        <span class="error <?= $errors_arr['MSACCESS'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label>
        </p>
        <p>
            <label><?= esc_html__('Κάτοχος Άδειας Οδήγησης*', 'exis'); ?><br>
                <span class="wpcf7-form-control-wrap OwnsVehicle">
                        <?php $vehicles = [0 => 'Τίποτα', 1 => 'Αυτοκίνητο/Μοτοσυκλέτα', 2 => 'Μόνο Αυτοκίνητο', 3 => 'Μόνο Μοτοσυκλέτα'];
                        $veh_opts = '';
                        foreach ($vehicles as $key => $veh) {
	                        $veh_opts .= '<option ';
	                        $veh_opts .= is_numeric($_POST['OwnsVehicle']) && $_POST['OwnsVehicle'] == $key ? "selected" : '';
	                        $veh_opts .= ' value="' . $key . '">' . $veh;
	                        $veh_opts .= '</option>';
                        } ?>
                        <select name="OwnsVehicle"
                                class="wpcf7-form-control wpcf7-select <?= $errors_arr['OwnsVehicle'] ? 'error_border' : ''; ?>"
                                required>
                            <option value="">---</option>
                            <?= $veh_opts; ?>
                        </select>
                        <span class="error <?= $errors_arr['OwnsVehicle'] ? 'visible' : ''; ?>"><?= $error ?></span>
                    </span>
            </label></p>
        <div id="details">
            <div class="ekpaideusi">
                <legend><?= esc_html__('Εκπαίδευση', 'exis'); ?></legend>
                <p>
                    <button class="button_minus" id="delete_education_history">-</button>
                    <button class="button_plus" id="add_education_history">+</button>
                </p>
                <div class="form-group education_history border-1">
                    <div class="clone-edu">
                        <div>
                            <p>
                                <label>
                                    <span class="education_type"><?= esc_html__('Τύπος Εκπαίδευσης*', 'exis'); ?></span><br>
									<?php $edu_sql = "SELECT * FROM education WHERE ID_EDU in (4,12,5,6,7,8,28)";
									$edu_opts = '';
									if ($result = $mysqli->query($edu_sql)) {
										while ($edu = $result->fetch_assoc()) {
											$edu_opts .= '<option ';
											$edu_opts .= $_POST['Details']['EducationHistory'][0]['Type'] == $edu['ID_EDU'] ? "selected" : '';
											$edu_opts .= ' value="' . $edu['ID_EDU'] . '">' . $edu['EDU_DESCR'];
											$edu_opts .= '</option>';
										}
										$result->free();
									}; ?>
                                    <span class="wpcf7-form-control-wrap edu_history_type">
                                            <select name="Details[EducationHistory][0][Type]"
                                                    class="wpcf7-form-control wpcf7-select <?= $errors_arr['EducationHistory_Type'] ? 'error_border' : ''; ?>"
                                                    id="edu_history_type" required>
                                                <option value="">---</option>
                                                <?= $edu_opts; ?>
                                            </select>
                                            <span class="error <?= $errors_arr['EducationHistory_Type'] ? 'visible' : ''; ?>"><?= $error ?></span>
                                        </span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <span class="education_institution"><?= esc_html__('Εκπαιδευτικό ίδρυμα*', 'exis'); ?></span><br>
                                    <span class="wpcf7-form-control-wrap edu_institution">
                                            <input type="text" name="Details[EducationHistory][0][Institution]"
                                                   value="<?= $_POST['Details']['EducationHistory'][0]['Institution'] ? $_POST['Details']['EducationHistory'][0]['Institution'] : '' ?>"
                                                   size="40"
                                                   class="wpcf7-form-control wpcf7-text <?= $errors_arr['EducationHistory_Institution'] ? 'error_border' : ''; ?>"
                                                   required>
                                                <span class="error <?= $errors_arr['EducationHistory_Institution'] ? 'visible' : ''; ?>"><?= $error ?></span>
                                        </span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <span class="education_title"><?= esc_html__('Τίτλος*', 'exis'); ?></span><br>
                                    <span class="wpcf7-form-control-wrap edu_title">
                                            <input type="text" name="Details[EducationHistory][0][Title]"
                                                   value="<?= $_POST['Details']['EducationHistory'][0]['Title'] ? $_POST['Details']['EducationHistory'][0]['Title'] : '' ?>"
                                                   size="40"
                                                   class="wpcf7-form-control wpcf7-text <?= $errors_arr['EducationHistory_Title'] ? 'error_border' : ''; ?>"
                                                   required>
                                                <span class="error <?= $errors_arr['EducationHistory_Title'] ? 'visible' : ''; ?>"><?= $error ?></span>
                                        </span>
                                </label>
                            </p>
                        </div>
                        <div>
                            <p>
                                <label>
                                    <span class="education_start_date"><?= esc_html__('Έτος έναρξης φοίτησης*', 'exis'); ?></span><br>
                                    <span class="wpcf7-form-control-wrap edu_start_date">
                                            <input type="number" name="Details[EducationHistory][0][StartDate]"
                                                   max="<?= date('Y'); ?>" required
                                                   value="<?= $_POST['Details']['EducationHistory'][0]['StartDate'] ? $_POST['Details']['EducationHistory'][0]['StartDate'] : ''; ?>"
                                                   class="wpcf7-form-control wpcf7-date <?= $errors_arr['EducationHistory_StartDate'] ? 'error_border' : ''; ?>"/>
                                                <span class="error <?= $errors_arr['EducationHistory_StartDate'] ? 'visible' : ''; ?>"><?= $errors_arr['EducationHistory_StartDate'] ?: $error; ?></span>
                                        </span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <span class="education_finish_year"><?= esc_html__('Έτος αποφοίτησης*', 'exis'); ?></span><br>
                                    <span class="wpcf7-form-control-wrap edu_finish_year">
                                            <input type="number" name="Details[EducationHistory][0][GraduationYear]"
                                                   max="<?= date('Y'); ?>"
                                                   value="<?= $_POST['Details']['EducationHistory'][0]['GraduationYear'] ? $_POST['Details']['EducationHistory'][0]['GraduationYear'] : '' ?>"
                                                   class="wpcf7-form-control wpcf7-number <?= $errors_arr['EducationHistory_GraduationYear'] ? 'error_border' : ''; ?>"
                                                   required>
                                                <span class="error <?= $errors_arr['EducationHistory_GraduationYear'] ? 'visible' : ''; ?>"><?= $errors_arr['EducationHistory_GraduationYear'] ?: $error; ?></span>
                                        </span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="glwsses">
                <legend><?= esc_html__('Ξένες Γλώσσες', 'exis'); ?></legend>
                <p>
                    <button class="button_minus" id="delete_languages_group">-</button>
                    <button class="button_plus" id="add_languages_group">+</button>
                </p>
                <div class="form-group languages_group border-1">
                    <div class="clone_lang">
                        <div>
                            <p>
                                <label>
                                    <span class="edu_lang"><?= esc_html__('Γλώσσα*', 'exis'); ?></span><br>
									<?php $langs = [1 => 'Αγγλικά', 2 => 'Γαλλικά', 3 => 'Γερμανικά', 4 => 'Ιταλικά', 5 => 'Ισπανικά', 6 => 'Ιαπωνικά', 7 => 'Κινέζικα',
										8 => 'Βουλγάρικα', 9 => 'Σερβοκροάτικα', 10 => 'Ρώσικα', 11 => 'Τούρκικα', 12 => 'Εβραϊκά', 13 => 'Ινδικά', 14 => 'Τσέχικα',
										15 => 'Ρουμανικά', 16 => 'Πορτογαλικά', 17 => 'Σουηδικά', 18 => 'Φιλανδικά', 19 => 'Νορβηγικά', 20 => 'Δανέζικα',
										21 => 'Ολλανδικά', 22 => 'Ελληνικά', 23 => 'Αραβικά', 24 => 'Αιγυπτιακά', 25 => 'Αλβανικά', 26 => 'Αυστριακά',
										27 => 'Κουρδικά', 29 => 'Ουγγρικά', 30 => 'Πολωνικά', 31 => 'Σερβικά'];
									$lang_opts = '';
									asort($langs);
									foreach ($langs as $key => $lang) {
										$lang_opts .= '<option ';
										$lang_opts .= $_POST['Details']['Languages'][0]['LangId'] == $key ? 'selected' : '';
										$lang_opts .= ' value="' . $key . '">' . $lang;
										$lang_opts .= '</option>';
									} ?>
                                    <span class="wpcf7-form-control-wrap education_lang">
                                            <select name="Details[Languages][0][LangId]"
                                                    class="wpcf7-form-control wpcf7-select <?= $errors_arr['Languages_LangId'] ? 'error_border' : ''; ?>"
                                                    id="langs" required>
                                                <option value="">---</option>
                                                <?= $lang_opts; ?>
                                            </select>
                                            <span class="error <?= $errors_arr['Languages_LangId'] ? 'visible' : ''; ?>"><?= $error ?></span>
                                        </span>
                                </label>
                            </p>
							<?php
							$levels = ["5" => "Μητρική", "4" => "Άριστο", "1" => "Πολύ καλό", "2" => "Καλό", "3" => "Μέτριο", "6" => "Βασικό"];
							$level_opts = '';
							foreach ($levels as $key => $val) {
								$level_opts .= '<option ';
								$level_opts .= $_POST['Details']['Languages'][0]['LangLevelRead'] ? 'selected' : ' ';
								$level_opts .= ' value="' . $key . '">';
								$level_opts .= $val . '</option>';
							} ?>
                            <p>
                                <label>
                                    <span class="edu_lang_read"><?= esc_html__('Επίπεδο*', 'exis'); ?></span><br>
                                    <span class="wpcf7-form-control-wrap education_lang_read">
                                            <select name="Details[Languages][0][LangLevelRead]"
                                                    class="wpcf7-form-control wpcf7-select <?= $errors_arr['Languages_LangLevelRead'] ? 'error_border' : ''; ?>"
                                                    id="lang_levels" required>
                                                <option value="">---</option>
                                                <?= $level_opts; ?>
                                            </select>
                                            <span class="error <?= $errors_arr['Languages_LangLevelRead'] ? 'visible' : ''; ?>"><?= $error ?></span>
                                        </span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="proigoumeni_ergasia">
        <h2 class="text-center"><?= esc_html__('Εργασιακη εμπειρια', 'exis'); ?></h2>
        <p>
            <button class="button_minus" id="delete_exp">-</button>
            <button class="button_plus" id="add_exp">+</button>
        </p>
        <div class="form-group previous_employment border-1">
            <div class="clone_exp">
                <div>
                    <p>
                        <label>
                            <span class="previous_exp_txt"><?= esc_html__('Προηγούμενος Εργοδότης', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_descr">
                                    <input type="text" name="Details[PreviousEmployment][0][EmployerDescr]" size="40"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['EmployerDescr'] ? $_POST['Details']['PreviousEmployment'][0]['EmployerDescr'] : '' ?>"
                                           class="wpcf7-form-control wpcf7-text <?= $errors_arr['PreviousEmployment_EmployerDescr'] ? 'error_border' : ''; ?>">
                                <span class="error <?= $errors_arr['PreviousEmployment_EmployerDescr'] ? 'visible' : ''; ?>"><? //= $nec_error;; ?></span>
                                </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <span class="previous_exp_city"><?= esc_html__('Πόλη', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_city">
                                    <input type="text" name="Details[PreviousEmployment][0][EmployerCityDescr]"
                                           size="40"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['EmployerCityDescr'] ? $_POST['Details']['PreviousEmployment'][0]['EmployerCityDescr'] : '' ?>"
                                           class="wpcf7-form-control wpcf7-text <?= $errors_arr['PreviousEmployment_EmployerCityDescr'] ? 'error_border' : ''; ?>">
                                <span class="error <?= $errors_arr['PreviousEmployment_EmployerCityDescr'] ? 'visible' : ''; ?>"><? //= $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <span class="previous_exp_country"><?= esc_html__('Χώρα', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_country">
                                    <select name="Details[PreviousEmployment][0][EmployerCountryID]"
                                            class="wpcf7-form-control wpcf7-select <?= $errors_arr['PreviousEmployment_EmployerCountryID'] ? 'error_border' : ''; ?>">
                                        <option value="">---</option>
                                        <?php countries($_POST['Details']['PreviousEmployment'][0]['EmployerCountryID'], $mysqli); ?>
                                    </select>
                                <span class="error <?= $errors_arr['PreviousEmployment_EmployerCountryID'] ? 'visible' : ''; ?>"><? //= $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                </div>
                <div>
                    <p>
                        <label>
                            <span class="previous_exp_tmima"><?= esc_html__('Τμήμα', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_tmima">
                                    <input type="text" name="Details[PreviousEmployment][0][DepartmentDescr]" size="40"
                                           class="wpcf7-form-control wpcf7-text <?= $errors_arr['PreviousEmployment_DepartmentDescr'] ? 'error_border' : ''; ?>"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['DepartmentDescr'] ? $_POST['Details']['PreviousEmployment'][0]['DepartmentDescr'] : '' ?>">
                                 <span class="error <?= $errors_arr['PreviousEmployment_DepartmentDescr'] ? 'visible' : ''; ?>"><? //= $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <span class="previous_exp_pos"><?= esc_html__('Θέση', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_pos">
                                    <input type="text" name="Details[PreviousEmployment][0][PositionDescr]" size="40"
                                           class="wpcf7-form-control wpcf7-text <?= $errors_arr['PreviousEmployment_PositionDescr'] ? 'error_border' : ''; ?>"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['PositionDescr'] ? $_POST['Details']['PreviousEmployment'][0]['PositionDescr'] : ''; ?>">
                                <span class="error <?= $errors_arr['PreviousEmployment_PositionDescr'] ? 'visible' : ''; ?>"><? //= $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <span class="previous_exp_from"><?= esc_html__('Από', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_from">
                                    <input type="date" name="Details[PreviousEmployment][0][FromDate]" min="1900-01-01"
                                           max="<?= date('Y-m-d'); ?>"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['FromDate'] ? $_POST['Details']['PreviousEmployment'][0]['FromDate'] : ''; ?>"
                                           class="wpcf7-form-control wpcf7-date <?= $errors_arr['PreviousEmployment_FromDate'] ? 'error_border' : ''; ?>">
                                <span class="error <?= $errors_arr['PreviousEmployment_FromDate'] ? 'visible' : ''; ?>"><? //= $errors_arr['PreviousEmployment_FromDate'] ?: $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                </div>
                <div>
                    <p>
                        <label>
                            <span class="previous_exp_to"><?= esc_html__('Έως', 'exis'); ?></span><br>
                            <span class="wpcf7-form-control-wrap prev_exp_to">
                                    <input type="date" name="Details[PreviousEmployment][0][ToDate]" min="1900-01-01"
                                           max="<?= date('Y-m-d'); ?>"
                                           value="<?= $_POST['Details']['PreviousEmployment'][0]['FromDate'] ? $_POST['Details']['PreviousEmployment'][0]['FromDate'] : ''; ?>"
                                           class="wpcf7-form-control wpcf7-date <?= $errors_arr['PreviousEmployment_ToDate'] ? 'error_border' : ''; ?>">
                                <span class="error <?= $errors_arr['PreviousEmployment_ToDate'] ? 'visible' : ''; ?>"><? //= $errors_arr['PreviousEmployment_ToDate'] ?: $nec_error; ?></span>
                                </span>
                        </label>
                    </p>
                </div>
            </div>
        </div>
		<?php
		$extra_fields_sql = "SELECT * FROM `hrm_extrafields` WHERE Descr IS NOT NULL";
		$inputs = '';
		if ($result = $mysqli->query($extra_fields_sql)) {
			$i = 0;
			while ($extra = $result->fetch_assoc()) {
				$inputs .= '<p><label>' . $extra['Descr'] . '*<br>';
				$inputs .= '<span class="wpcf7-form-control-wrap ExtraField">';
				$inputs .= '<input type="hidden" name="Details[ExtraFields][' . $i . '][FieldCode]" value="' . $extra['Code'] . '"/>';
				$inputs .= '<input type="number" name="Details[ExtraFields][' . $i . '][FieldValue]" class="wpcf7-form-control wpcf7-number ';
				$inputs .= $errors_arr['Details_ExtraFields_' . $extra['Code']] ? 'error_border' : '' . '" value="';
				$inputs .= is_numeric($_POST['Details']['ExtraFields'][$i]['FieldValue']) ? $_POST['Details']['ExtraFields'][$i]['FieldValue'] : '';
				$inputs .= '" min="0" required /></span>';
				$inputs .= '<span class="error ';
				$inputs .= $errors_arr['Details_ExtraFields_' . $extra['Code']] ? 'visible' : '';
				$inputs .= '">' . $error . '</span>';
				$inputs .= '</label></p>';
				$i++;
			}
			$result->free();
		};
		echo $inputs; ?>
        <div>
            <label>
                <span><?php esc_html_e('Βιογραφικό σημείωμα', 'exis'); ?></span><br>
                <input type="file" name="CVURL" class="wpcf7-form-control"/>
                <span class="error <?= $errors_arr['CVURL'] ? 'visible' : ''; ?>"><?= $errors_arr['CVURL'] ? $errors_arr['CVURL'] : $error; ?></span>
            </label>
        </div>
    </div>

    <div class="consent_submit <?= $errors_arr['consent'] ? 'error_border' : ''; ?>">
        <label>
            <input type="checkbox" name="consent" value="1" required/>
			<?= __('Πριν προχωρήσετε με την υποβολή του βιογραφικού σας, σας παρακαλούμε να διαβάσετε με προσοχή τη δήλωση προστασίας προσωπικών δεδομένων της Δικηγορικής Εταιρίας Σιούφας & Συνεργάτες που θα βρείτε 
			<a href="' . get_permalink(1646) . '" target="_new">εδώ</a>.', 'exis'); ?>
        </label>
    </div>
    <span class="error <?= $errors_arr['consent'] ? 'visible' : ''; ?>"><?= $errors_arr['consent']; ?></span><br>
    <input type="submit" value="Υποβολή" class="wpcf7-form-control wpcf7-submit submit_cv" id="submit">
</form>