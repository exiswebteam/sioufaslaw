<?php
get_header();
require_once "career-form-config.php"; ?>
    <div class="container">
		<?php if (!empty($errors_arr)) { ?>
            <ul class="invalid">
                <li><?= esc_html__('Η φόρμα δεν είναι έγκυρη', 'exis'); ?></li>
            </ul>
		<?php }
		if (!$success && !empty($_POST) && empty($errors_arr)) { ?>
            <ul class="invalid">
                <li><?= esc_html__('Υπήρξε πρόβλημα κατά την καταχώριση, παρακαλώ προσπαθείστε πάλι', 'exis'); ?></li>
            </ul>
			<?php
		} else if ($success) { ?>
            <div class="succeed">
                <ul class="valid">
                    <li><?= esc_html__('Συγχαρητήρια το βιογραφικό σας καταχωρήθηκε', 'exis'); ?></li>
                </ul>
            </div>
		<?php }
		require_once "submit_cv_form.php";?>
    </div>
    </div>
    </div>
<?php mysqli_close($mysqli);
get_footer(); ?>