<?php
$mysqli = mysqli_connect('sftp.sioufaslaw.gr:8888', 'exis', 'exisGSLO!@#', 'exis_hr');
$extra_fields_sql = "SELECT * FROM `hrm_extrafields` WHERE Descr IS NOT NULL";

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
	$sort_col = [];
	foreach ($arr as $key => $row) {
		$sort_col[$key] = $row[$col];
	}
	array_multisort($sort_col, $dir, $arr);
}

function yes_no($selected) {
	$opts = '';
	$opt_arr = [0 => 'Όχι', 1 => 'Ναι'];
	foreach ($opt_arr as $key => $val) {
		$opts .= '<option value="' . $key . '" ';
		$opts .= is_numeric($selected) && $selected == $key ? ' selected ' : '';
		$opts .= '>' . $val . '</option>';
	}
	echo $opts;
}

function countries($selected, $mysqli) {
	$countries_sql = "SELECT * FROM countries WHERE ID_YEKA IS NOT NULL";
	$countries = '';
	if ($result = $mysqli->query($countries_sql)) {
		$countr_arr = $result->fetch_all(MYSQLI_ASSOC);
		array_sort_by_column($countr_arr, 'COUNTRY_GR');
		foreach ($countr_arr as $country) {
			$countries .= '<option ';
			$countries .= strlen($selected) > 0 && $selected == $country['CODE'] ? ' selected ' : '';
			$countries .= ' value="' . $country['CODE'] . '">' . $country['COUNTRY_GR'];
			$countries .= '</option>';
		}
		$result->free();
	}
	echo $countries;
}

$error = __('Το πεδίο είναι υποχρεωτικό', 'exis');
$nec_error = __('Για συμπλήρωση προηγούμενου εργοδότη, όλα τα πεδία είναι υποχρεωτικά', 'exis');

if (!empty($_POST)) {
	$success = false;
	$errors_arr = [];
	$it = 0;
	$extra_required = [01, 02];
	if ($result = $mysqli->query($extra_fields_sql)) {
		while ($extra = $result->fetch_assoc()) {
			if (in_array($_POST['Details']['ExtraFields'][$it]['FieldCode'], $extra_required) && '' == $_POST['Details']['ExtraFields'][$it]['FieldValue']) {
				$errors_arr['Details_ExtraFields_' . $extra['Code']] = __('Το πεδίο είναι υποχρεωτικό', 'exis');
			}
			$it++;
		}
		$result->free();
	}
	$single_required = [
		'FirstName' => __('Όνομα', 'exis'),
		'LastName' => __('Επώνυμο', 'exis'),
		'FathersName' => __('Όνομα πατρός', 'exis'),
		'BirthDate' => __('Ημερομηνία γέννησης', 'exis'),
		'MaritalType' => __('Οικογενειακή κατάσταση', 'exis'),
		'CountryOfResidenceCode' => __('Χώρα Διαμονής', 'exis'),
		'email' => __('e-mail', 'exis'),
		'Mobile' => __('Κινητό', 'exis'),
		'Telephone' => __('Τηλέφωνο', 'exis'),
		'AddressStreet' => __('Διεύθυνση', 'exis'),
		'AddressNumber' => __('Αριθμός', 'exis'),
		'AddressCity' => __('Πόλη', 'exis'),
		'AddressZipCode' => __('Tαχυδρομικός Κώδικας', 'exis'),
		'Area' => __('Περιοχή', 'exis'),
		'HasComputerKnowledge' => __('Γνώση υπολογιστών', 'exis'),
		'HasECDL' => __('ECDL', 'exis'),
		'MSEXCEL' => __('Γνώση Excel', 'exis'),
		'MSPOWERPOINT' => __('Γνώσεις PowerPoint', 'exis'),
		'MSACCESS' => __('Γνώσεις MS Access', 'exis'),
		'OwnsVehicle' => __('Κάτοχος Άδειας Οδήγησης', 'exis'),
	];
	/**
	 * @param $date
	 * @param $key
	 * @return bool
	 */
	function validate_date($date, $key) {
		$year = explode("-", $date[$key])[0];
		if ($year < 1900 || $year > date('Y')) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param $email
	 * @return bool
	 */
	function validate_email($email) {
		$v = "/[a-zA-Z0-9_\-.+]+@[a-zA-Z0-9-]+.[a-zA-Z]+/";
		return (bool)preg_match($v, $email);
	}

	foreach ($single_required as $key => $val) {
		if (strlen($_POST[$key]) < 1) {
			$errors_arr[$key] = __('Το πεδίο είναι υποχρεωτικό', 'exis');
		}
		if (($key === 'Telephone' || $key === 'Mobile') && !is_numeric($_POST[$key]) && !$errors_arr[$key]) {
			$errors_arr[$key] = __('Το πεδίο δέχεται μόνο αριθμούς', 'exis');
		}
		if (($key === 'Telephone' || $key === 'Mobile') && strlen($_POST[$key]) !== 10 && !$errors_arr[$key]) {
			$errors_arr[$key] = __('Το πεδίο δέχεται μέχρι 10 αριθμούς', 'exis');
		}
		if ($key === 'AddressZipCode' && !is_numeric($_POST[$key]) && !$errors_arr[$key] && strlen($_POST[$key]) !== 5) {
			$errors_arr[$key] = __('Το πεδίο δέχεται μέχρι 5 αριθμούς', 'exis');
		}
		if ($key === 'BirthDate' && !$errors_arr[$key] && !validate_date($_POST, $key)) {
			$errors_arr[$key] = __('Το έτος πρέπει να είναι μεταξύ 1900 και ' . date('Y'), 'exis');
		}
		if ($key === 'email' && !$errors_arr[$key] && !filter_var($_POST[$key], FILTER_VALIDATE_EMAIL)) {
			$errors_arr[$key] = __('Λανθασμένο e-mail.', 'exis');
		}
	}

	if ($_POST['HasECDL'] == 1 && ($_POST['ECDLRank'] > 99 || $_POST['ECDLRank'] < 1)) {
		$errors_arr['ECDLRank'] = __('Ο βαθμός πρέπει να είναι μεταξύ 1 και 99', 'exis');
	}
	if ($_POST['HasECDL'] == 1 && '' == $_POST['ECDLRank']) {
		$errors_arr['ECDLRank'] = __('Αν έχετε ECDL, το πεδίο βαθμού είναι υποχρεωτικό', 'exis');
	}

	$edu_required = [
		'Type' => __('Τύπος Εκπαίδευσης', 'exis'),
		'Institution' => __('Εκπαιδευτικό ίδρυμα', 'exis'),
		'Title' => __('Τίτλος', 'exis'),
		'StartDate' => __('Έτος έναρξης φοίτησης', 'exis'),
		'GraduationYear' => __('Έτος αποφοίτησης', 'exis'),
	];
	foreach ($edu_required as $key => $val) {
		if (strlen($_POST['Details']['EducationHistory'][0][$key]) < 1) {
			$errors_arr['EducationHistory_' . $key] = __('Το πεδίο είναι υποχρεωτικό', 'exis');
		}
		if (($key == 'GraduationYear' || $key == 'StartDate') && !$errors_arr['EducationHistory_' . $key] && !validate_date($_POST['Details']['EducationHistory'][0], $key)) {
			$errors_arr['EducationHistory_' . $key] = __('Το έτος πρέπει να είναι μεταξύ 1900 και ' . date('Y'), 'exis');
		}
	}
	$langs_required = [
		'LangId' => __('Γλώσσα', 'exis'),
		'LangLevelRead' => __('Επίπεδο', 'exis'),
	];
	foreach ($langs_required as $key => $val) {
		if (strlen($_POST['Details']['Languages'][0][$key]) < 1) {
			$errors_arr['Languages_' . $key] = __('Το πεδίο είναι υποχρεωτικό', 'exis');
		}
	}
	/*
		foreach ($_POST['Details']['PreviousEmployment'] as $key => $detail) {
			foreach ($detail as $val) {
				$to_unset = array_search("", $val, TRUE);
				if ($to_unset) unset($_POST['Details']['PreviousEmployment'][0][$key]);
			}
		}
	*/
	if (!empty($_POST['Details']['PreviousEmployment'][0])) {
		$i = 0;
		foreach ($_POST['Details']['PreviousEmployment'] as $prev) {
			foreach ($prev as $key => $val) {
//				if ('' == $val || !isset($val)) {
//					$errors_arr['PreviousEmployment_' . $key] = $nec_error;
//				}
				if (($key == 'FromDate' || $key == 'ToDate') && !validate_date($_POST['Details']['PreviousEmployment'][$i], $key) && strlen($_POST['Details']['PreviousEmployment'][$i][$key]) > 0) {
					//&& empty($errors_arr['PreviousEmployment_' . $key])
					$errors_arr['PreviousEmployment_' . $key] = __('Το έτος πρέπει να είναι μεταξύ 1900 και ' . date('Y'), 'exis');
				}
			}
			$i++;
		}
	}
	if (!isset($_POST['consent'])) $errors_arr['consent'] = __('Χρειάζεται η συναίνεση σας για να πραγματοποιηθεί η αποστολή της φόρμας.', 'exis');

	require(dirname(__FILE__) . '/../../../wp-load.php');
	$wordpress_upload_dir = wp_upload_dir();
	$allowed_exts = ['docx', 'pdf', 'doc'];
	$uploadedfile = $_FILES['CVURL'];
	if ($_FILES['CVURL']['name'] !== '') {
		$new_file_mime = wp_check_filetype($uploadedfile['name']);
		if ($_FILES['CVURL']['size'] > 3000000) $errors_arr['CVURL'] = __('Το αρχείο να μην είναι μεγαλύτερο από 3MB', 'exis');
		if (!in_array($new_file_mime['ext'], $allowed_exts) && empty($errors_arr['CVURL'])) $errors_arr['CVURL'] = __('Δεν επιτρέπεται αυτός ο τύπος αρχείου, δοκιμάστε πάλι (pdf,doc)', 'exis');
	}
	/*
	if (!empty($_POST['Positions'])) {
		$i = 0;
		$positions = [];
		foreach ($_POST['Positions'] as $position) {
			$positions['Details']['Positions'][]['ID_POS'] = intval($position);
			$i++;
		}
		unset($_POST['Positions']);
		$_POST = array_merge($_POST, $positions);
	}
	*/
//	print_r(json_encode($_POST));
//	print_r(json_encode($positions));
	//IF not errors
	if (empty($errors_arr)) {
		//upload file
		if ($_FILES['CVURL']['name'] !== '') {
			$new_file_path = $wordpress_upload_dir['path'] . '/' . $uploadedfile['name'];
			$i = 1;
			while (file_exists($new_file_path)) {
				$i++;
				$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '' . $uploadedfile['name'];
			}
			if (move_uploaded_file($uploadedfile['tmp_name'], $new_file_path)) {
				$upload_id = wp_insert_attachment([
					'guid' => $new_file_path,
					'post_mime_type' => $new_file_mime['ext'],
					'post_title' => preg_replace('/\.[^.]+$/', '', $uploadedfile['name']),
					'post_content' => '',
					'post_status' => 'inherit',
				], $new_file_path);
				require_once(ABSPATH . 'wp-admin/includes/image.php');
				wp_update_attachment_metadata($upload_id, wp_generate_attachment_metadata($upload_id, $new_file_path));
				$_POST['CVURL'] = trim(wp_get_attachment_url($upload_id));
				$_POST['CVURL'] = str_replace('://', '://www.', $_POST['CVURL']);
				echo $_POST['CVURL'];
			}
		}

		$_POST['ForceOverride'] = true;
		unset($_POST['consent']);
		if (empty($_POST['Details'])) $_POST['Details'] = new stdClass();
		if (!$_POST['CompanyID']) $_POST['CompanyID'] = 6;

		//parsing the year into date from number to date
		$i = 0;
		if ($_POST['Details']['EducationHistory'][$i]['StartDate']) {
			foreach ($_POST['Details']['EducationHistory'] as $edu) {
				$_POST['Details']['EducationHistory'][$i]['StartDate'] = $edu['StartDate'] . '-01-01';
				$i++;
			}
		}

		//unset empty values
		foreach ($_POST['Details'] as $key => $detail) {
			foreach ($detail as $val) {
				$to_unset = array_search("", $val, TRUE);
				if ($to_unset) unset($_POST['Details'][$key]);
			}
		}

		$send = json_encode($_POST);
		$url = 'http://193.92.160.28:30569/submitcv';
		$args = [
			'timeout' => 5,
			'httpversion' => '1.1',
			'headers' => [
				'Accept' => 'application/json',
				'Content-Type' => 'application/json',
			],
			'body' => $send,
		];

		echo "<h1>SEND OBJECT</h1>";
		print_r($send);
		$response = wp_remote_post($url, $args);
		if (!is_wp_error($response)) $responseCode = json_decode($response['body'])->responseCode;
		echo "<br><h1>RESPONSE</h1>";
		print_r($response);

		if (is_wp_error($response)) {
			$error_message = $response->get_error_message();
		} else if ($responseCode == '2' || $responseCode == '3') {
			$success = true;
			unset($_POST);
		}
	}
}