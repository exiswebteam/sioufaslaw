<?php get_header();
$mysqli = mysqli_connect('sftp.sioufaslaw.gr:8888', 'exis', 'exisGSLO!@#', 'exis_hr');
$jobs_sql = "SELECT * FROM `position`"; ?>
    <h1 class="text-center"><?= __('Καριέρα', 'exis'); ?></h1>
    <div class="container">
        <div class="content-area">
            <?php the_content(); ?>
        </div>
        <?php
		if ($result = $mysqli->query($jobs_sql)) { ?>
            <div class="row">
			<?php while ($job = $result->fetch_assoc()) { ?>
                <div class="job-card col">
                    <h3><?= $job['DESCR']; ?></h3>
                    <a href="<?= get_home_url() . "/job/?position=" . $job['ID_POS'] ?>"><?= __('Στείλε Βιογραφικό', 'exis'); ?></a>
                </div>
			<?php }
			$result->free(); ?>
            </div>
		<?php } else { echo __('There are no Jobs at this moment', 'exis');}?>		
    </div>
    </div>
    </div>
<?php
mysqli_close($mysqli);
get_footer();
