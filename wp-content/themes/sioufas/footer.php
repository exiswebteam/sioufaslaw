<?php
if(is_page(23)){?>

   <div class="popup-terms politiki-popup">
       <div class="popup-terms__inner">
           <div>
               <div>
                   <h2><?php echo get_the_title(1646) ?></h2>
                   <?php
                   echo apply_filters('the_content', get_post_field('post_content', 1646));
                   ?>
               </div>
               <h2 class="privacy-popup-title"><?php echo get_the_title(1639) ?></h2>
           <?php
            echo apply_filters('the_content', get_post_field('post_content', 1639));
           ?>
           </div>
           <div class="terms-btn politiki-popup__btn">
               <div>
                   <?php echo __('Το κατάλαβα', 'sioufas'); ?>
               </div>
           </div>
       </div>
   </div>


    <div class="politiki-popup--news">
        <div class="popup-terms__inner">

            <div>

                <div>
                    <h2><?php echo get_the_title(1644) ?></h2>
                    <?php
                    echo apply_filters('the_content', get_post_field('post_content', 1644));
                    ?>
                </div>

                <h2 class="privacy-popup-title"><?php echo get_the_title(1639) ?></h2>
                <?php
                echo apply_filters('the_content', get_post_field('post_content', 1639));
                ?>
            </div>
            <div class="terms-btn politiki-popup__btn--news">
                <div>
                    <?php echo __('Το κατάλαβα', 'sioufas'); ?>
                </div>
            </div>
        </div>
    </div>



<?php
}
?>

<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
        <?php // Sioufas Bottom 100% ?>
        <?php if ( is_active_sidebar( 'footer_top_100' ) ) : ?>
          <div id="footer-top" class="container clearfix">
          <?php dynamic_sidebar( 'footer_top_100' ); ?>
          </div>
        <?php endif; ?>
<!--
        <?php // Sioufas Mid 50% 1 ?>
        <?php if ( is_active_sidebar( 'footer_mid_50_1' ) ) : ?>
          <div class="container clearfix">
          <div id="footer-mid-left">
          <?php dynamic_sidebar( 'footer_mid_50_1' ); ?>
          </div>
        <?php endif; ?>
        <?php // Sioufas Mid 50% 2 ?>
        <?php if ( is_active_sidebar( 'footer_mid_50_2' ) ) : ?>
          <div id="footer-mid-right">
          <?php dynamic_sidebar( 'footer_mid_50_2' ); ?>
          </div>
          </div>
        <?php endif; ?>
        <?php // Sioufas Bottom 100% ?>
        <?php if ( is_active_sidebar( 'footer_bot_100' ) ) : ?>
          <div id="footer-bot" class="container clearfix">
          <?php dynamic_sidebar( 'footer_bot_100' ); ?>
          </div>
        <?php endif; ?>
-->
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>
