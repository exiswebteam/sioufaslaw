<?php get_header();
if (isset($_GET['position'])) {
	require_once "career-form-config.php";
	session_start();
	$pos_id = sanitize_text_field($_GET['position']);
	$job_sql = "SELECT * FROM `position` WHERE `ID_POS` = " . mysqli_real_escape_string($mysqli, $pos_id); ?>
    <h1 class="text-center"><?= esc_html__('Θέση Εργασίας', 'exis'); ?></h1>
    <div class="container">
        <div class="content-area">
			<?php the_content(); ?>
        </div>
		<?php
		if ($result = $mysqli->query($job_sql)) { ?>
            <div class="row">
				<?php
				while ($job = $result->fetch_assoc()) {
					$_SESSION['ID_CMP'] = $job['ID_CMP']; ?>
                    <div class="job-card col">
                        <h3><?= $job['DESCR']; ?></h3>
                    </div>
				<? }
				$result->free(); ?>
            </div>
			<?php if (!empty($errors_arr)) { ?>
                <ul class="invalid">
                    <li><?= esc_html__('Η φόρμα δεν είναι έγκυρη', 'exis'); ?></li>
                </ul>
			<?php }
			//form template
			if (!$success && !empty($_POST) && empty($errors_arr)) { ?>
                <ul class="invalid">
                    <li><?= esc_html__('Υπήρξε πρόβλημα κατά την καταχώριση, παρακαλώ προσπαθείστε πάλι', 'exis'); ?></li>
                </ul>
				<?php
			} else if ($success) { ?>
                <div class="succeed">
                    <ul class="valid">
                        <li><?= esc_html__('Συγχαρητήρια το βιογραφικό σας καταχωρήθηκε', 'exis'); ?></li>
                    </ul>
                </div>
			<?php }
			require_once "submit_cv_form.php";?>
		<?php } else {
			echo __('There are no Jobs at this moment', 'exis');
		} ?>
    </div>
    </div>
	<?php mysqli_close($mysqli);
} else {
	wp_redirect(home_url() . "/jobtest/", 301);
} ?>
    </div><!-- additional divs due to bug of theme in closing tags -->
    </div>
<?php get_footer(); ?>